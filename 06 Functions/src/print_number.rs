
fn main() {
  print_number(5);
  print_sum(5, 5);
}

fn print_number(x: int) {
  println!("x is: {}", x);
}

fn print_sum(x: int, y: int) {
  println!("sum is: {}", x + y);
}
