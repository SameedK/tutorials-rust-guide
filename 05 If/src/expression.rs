
fn is_five(x: int) -> bool {
  if x == 5i { true } else { false }
}

fn main() {
  println!("Is  5 equal to 5? {}", is_five( 5i));
  println!("Is 10 equal to 5? {}", is_five(10i));
}
